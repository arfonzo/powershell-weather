# PowerShell Weather #

## What is this thing? ##

Current conditions and forecast.

You need to register for an OpenWeatherMap API key for free, here:

* https://home.openweathermap.org/users/sign_up

## Installation ##

* Put the .psm1 somewhere, like the directory where your PowerShell $profile resides, for example.

* Put the config file in your $home directory:

```
#!powershell
PS C:\...\PS_Weather> cp .\Get-Weather_config.json $HOME
```

* Modify the configuration file with your API key and preferred location.

* Import it:

```
#!powershell
Import-Module C:\some\path\to\Get-Weather.psm1
```

## Parameters ##
```
[string]  -APIKey     # Use API key [string]. Required, or must be set in configuration file.
[switch]  -Forecast   # Get weekly forecast.
[switch]  -Formatted  # Format output.
[switch]  -Full       # Show full details.
[string]  -Location   # Use Location [string].
[switch]  -Night      # Night mode: filter daytimes out.
[string]  -Units      # Set units to "metric" (C, default), "imperial" (F) or "standard" (K).
[switch]  -Verbose    # Show more crap (for debugging and nerdiness).
[switch]  -Xml        # Return output as an XML object.
```


## Examples ##

### Current Conditions ###

By default, Get-Weather will check for a config file which has your preferred location set. You can also check the current conditions for a specific place using -Location:

```
#!powershell

PS > Get-Weather -Location London


coord      : @{lon=-0.13; lat=51.51}
weather    : {@{id=721; main=Haze; description=haze; icon=50n}}
base       : stations
main       : @{temp=3.89; pressure=1018; humidity=75; temp_min=2; temp_max=6}
visibility : 10000
wind       : @{speed=2.6; deg=200}
clouds     : @{all=68}
dt         : 1482276000
sys        : @{type=1; id=5091; message=1.0006; country=GB; sunrise=1482221039; sunset=1482249220}
id         : 2643743
name       : London
cod        : 200
```

### Forecast ###

Get a 5-day forecast.

```
#!powershell

Get-Weather -Forecast -Location London -Formatted

DateTime            CloudPercentage Temperature
--------            --------------- -----------
2016-12-21 03:00:00              92        9.33
2016-12-21 06:00:00              76        9.14
2016-12-21 09:00:00              92         9.8
2016-12-21 12:00:00              92       10.61
...(snip)...
2016-12-25 15:00:00              76       13.99
2016-12-25 18:00:00              12       10.27
2016-12-25 21:00:00              20        8.28

```

You can use -Full to get some more details:

```
#!powershell

Get-Weather -Forecast -Formatted -Full

  Weekly Forecast


Date & Time         Cloudiness (%) Temperature (metric) Temp. Min. Temp. Max. Humidity (%)
-----------         -------------- -------------------- ---------- ---------- ------------
2017-02-09 18:00:00             88                 1.43       1.43       2.19           88
2017-02-09 21:00:00             88                 1.41       1.41       1.91           92
2017-02-10 00:00:00             88                 1.63       1.63       1.88           96
2017-02-10 03:00:00             88                  1.6        1.6        1.6           96
...(snip)...
2017-02-14 09:00:00             12                 3.51       3.51       3.51           90
2017-02-14 12:00:00              0                  8.6        8.6        8.6           80
2017-02-14 15:00:00              0                 10.4       10.4       10.4           77

```


### Astronomer? Check the Nights Ahead ###

Use the -Night switch to find suitable times to make love to your telescope. It filters out daytime forecasts.


```
#!powershell

PS > Get-Weather -Forecast -Night -Formatted

DateTime            CloudPercentage
--------            ---------------
2016-12-21 03:00:00 92%            
2016-12-21 18:00:00 100%           
2016-12-21 21:00:00 44%            
2016-12-22 00:00:00 0%             # Good view
2016-12-22 03:00:00 0%             
2016-12-22 18:00:00 24%            
2016-12-22 21:00:00 12%            # Decent view
2016-12-23 00:00:00 0%             
2016-12-23 03:00:00 36%            
2016-12-23 18:00:00 92%            
2016-12-23 21:00:00 92%            
2016-12-24 00:00:00 24%            
2016-12-24 03:00:00 80%            
2016-12-24 18:00:00 64%            
2016-12-24 21:00:00 88%            
2016-12-25 00:00:00 92%            # Terrible view
2016-12-25 03:00:00 100%           
2016-12-25 18:00:00 12%            
2016-12-25 21:00:00 20%            
```


### PowerShell Object Fun ###

You can play with the data in a multitude of ways. Use the -Xml parameter to output an object. You can use and abuse the object, or pass it on to integrate with anything you like.

Examples for your consideration:

```
#!powershell

PS > $london = Get-Weather -Location London -Xml
PS > $london.main.temp
3.89

PS > $london.sys.sunset # Sunset, in unix epoch time format
1482249220

PS > [TimeZone]::CurrentTimeZone.ToLocalTime(([datetime]'1/1/1970').AddSeconds($london.sys.sunset)) # Convert sunset to normal time

20 December 2016 15:53:40
```