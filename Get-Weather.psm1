﻿# Weather
#	Current conditions and forecast.
#	Author: arfonzo@gmail.com
#
#	You need to register for an API key for free, here:
#	  https://home.openweathermap.org/users/sign_up
#
function Get-Weather {
    param (
        [string]$APIKey,
        [switch]$Forecast,
        [switch]$Formatted,
        [switch]$Full,
        [string]$Location,
        [switch]$Night,
        [string]$Units,
        [switch]$Verbose,
        [switch]$Xml
    );

    # Load defaults from config file.
    $config_file = "$HOME\Get-Weather_config.json";
    if ($Verbose) { Write-Host "Checking for config file: $config_file ... " -NoNewline -ForegroundColor Yellow; }
    if (Test-Path $config_file) {
        if ($Verbose) { Write-Host "Found." -ForegroundColor Green; }
        if ($Verbose) { Write-Host "Loading configuration:" -ForegroundColor Yellow; }
        
        $json_config = Get-Content $config_file | ConvertFrom-Json;

        if ($json_config.APIKey -ne $null -and !$APIKey) {
            if ($Verbose) { Write-Host "  - Found API key: $($json_config.APIKey)" -ForegroundColor Green; }
            $APIKey = $json_config.APIKey;
        }

        if ($json_config.Location -ne $null -and !$Location) {
            if ($Verbose) { Write-Host "  - Found default location: $($json_config.Location)" -ForegroundColor Green; }
            $Location = $json_config.Location;
        }

    } else {
        if ($Verbose) { Write-Host "Not found."; }
    }

    # Night mode implies forecast mode.
    if ($Night) {
        $Forecast = $true;
    }

    if ($Forecast) {
        $url_openweather = "http://api.openweathermap.org/data/2.5/forecast?";
    } else {
        $url_openweather = "http://api.openweathermap.org/data/2.5/weather?";
    }


    if ($Location) {
        $loc = $Location;
    } else {
        $loc = "London";
    }

    if ($Units) {
        $unit = $Units;
    } else {
        $unit = "metric";
    }

    if ($APIKey) {
        $appid = $APIKey;
    } else {
        Write-Error "Requires -APIKey parameter.";
        return;
    }

    $url = $url_openweather + 'appid=' + $appid + '&q=' + $loc + '&units=' + $unit;

    if ($Verbose) {
        Write-Host "Querying URL: $url";
    }

    $json = Invoke-RestMethod -Uri $url;

    if ($Formatted -and !$Forecast -and !$Night) {
        # Formatted Current Conditions

        # Draw header
        Write-Host "                      " -BackgroundColor DarkCyan;
        Write-Host "  Current Conditions  " -ForegroundColor Green -BackgroundColor DarkCyan;
        Write-Host "                      " -BackgroundColor DarkCyan;

        $list = @{};
        $list.Add("name",$json.name);
        $list.Add("coord",$json.coord);
        $list.Add("weather",$json.weather.description);
        $list.Add("temp",$json.main.temp);
        $list.Add("temp_min",$json.main.temp_min);
        $list.Add("temp_max",$json.main.temp_max);
        $list.Add("pressure",$json.main.pressure);
        $list.Add("humidity",$json.main.humidity);
        $list.Add("visibility",$json.visibility);
        $list.Add("wind",$json.wind);
        $list.Add("clouds",$json.clouds.all);
        $list.Add("sys",$json.sys);
        if ($Verbose) {
            $list.Add("code",$json.cod);
        }
    
        return $list.GetEnumerator() | Sort-Object Name;

    } elseif ($Formatted -and !$Night) {
        # Forecast, Formatted, No Nights only.

        # Draw header
        Write-Host "                   " -BackgroundColor DarkCyan;
        Write-Host "  Weekly Forecast  " -ForegroundColor Green -BackgroundColor DarkCyan;
        Write-Host "                   " -BackgroundColor DarkCyan;

        if ($Full) {
            return $json.list | Format-Table @{Label="Date & Time"; Expression={$_.dt_txt}},@{Label="Cloudiness (%)"; Expression={$_.clouds.all}},@{Label="Temperature ($unit)";Expression={$_.main.temp}},@{Label="Temp. Min.";Expression={$_.main.temp_min}},@{Label="Temp. Max.";Expression={$_.main.temp_max}},@{Label="Humidity (%)";Expression={$_.main.humidity}};
        } else {
            return $json.list | Format-Table @{Label="Date & Time"; Expression={$_.dt_txt}},@{Label="Cloudiness (%)"; Expression={$_.clouds.all}},@{Label="Temperature ($unit)";Expression={$_.main.temp}};
        }
    } elseif ($Formatted -and $Night) {
        # Forecast, Formatted, Nights only.
        $table = New-Object System.Data.DataTable "Night Visibility";
        $col1 = New-Object System.Data.DataColumn "Date & Time",([string]);
        $col2 = New-Object System.Data.DataColumn Cloudiness,([string]);

        $table.Columns.Add($col1);
        $table.Columns.Add($col2);

        $json.list | ForEach-Object {
            if ($_.dt_txt.EndsWith("00:00:00") -or $_.dt_txt.EndsWith("03:00:00") -or $_.dt_txt.EndsWith("18:00:00") -or $_.dt_txt.EndsWith("21:00:00")) {      
                $row = $table.NewRow();
                $row."Date & Time" = $_.dt_txt; 
                $row.Cloudiness = "$($_.clouds.all)%";
                $table.Rows.Add($row);
            }
        }

        # Draw header
        Write-Host "            " -ForegroundColor Yellow -BackgroundColor DarkGray;
        Write-Host " Night Mode " -ForegroundColor Yellow -BackgroundColor DarkGray;
        Write-Host "            " -ForegroundColor Yellow -BackgroundColor DarkGray;

        return $table | Format-Table -AutoSize;

    }else {
        return $json
    }
}